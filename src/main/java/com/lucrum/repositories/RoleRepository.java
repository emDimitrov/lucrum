package com.lucrum.repositories;

import com.lucrum.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
    Role findRoleByAuthority (String authority);
    Role findRoleById (String id);
}
