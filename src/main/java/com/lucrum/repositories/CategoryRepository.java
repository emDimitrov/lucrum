package com.lucrum.repositories;

import com.lucrum.entities.Category;
import com.lucrum.model.service.CategoryServiceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {
    Optional<Category> findCategoryById(String id);
}
