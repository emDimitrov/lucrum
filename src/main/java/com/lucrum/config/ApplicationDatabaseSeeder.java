package com.lucrum.config;

import com.lucrum.entities.Role;
import com.lucrum.entities.User;
import com.lucrum.enums.UserRole;
import com.lucrum.model.binding.user.UserRegisterBindingModel;
import com.lucrum.model.service.UserServiceModel;
import com.lucrum.services.RoleService;
import com.lucrum.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
public class ApplicationDatabaseSeeder {

    private final UserService userService;
    private final JdbcTemplate jdbcTemplate;
    private final RoleService roleService;
    private final ModelMapper modelMapper;

    @Autowired
    public ApplicationDatabaseSeeder(JdbcTemplate jdbcTemplate, UserService userService, RoleService roleService, ModelMapper modelMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userService = userService;
        this.roleService = roleService;
        this.modelMapper = modelMapper;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedRolesTable();
        seedUsersTable();
    }

    private void seedRolesTable() {
        String sql = "SELECT authority FROM roles";
        List<String> result = jdbcTemplate.query(sql, (resultSet, rowNum) -> null);
        if(result == null || result.size() <= 0) {
            Role root = new Role(UserRole.ROOT.name());
            Role admin = new Role(UserRole.ADMIN.name());
            Role moderator = new Role(UserRole.MODERATOR.name());
            Role user = new Role(UserRole.USER.name());

            roleService.create(root);
            roleService.create(admin);
            roleService.create(user);
            roleService.create(moderator);
        }
    }

    private void seedUsersTable() {
        String sql = "SELECT * FROM Users";
        List<User> result = jdbcTemplate.query(sql, (resultSet, rowNum) -> null);

        if(result == null || result.size() <= 0) {
            Set<Role> authorities = new HashSet<Role>(){{
                add(roleService.getRoleByUserRole(UserRole.ROOT));
            }};

            UserRegisterBindingModel rootUser = new UserRegisterBindingModel("admin","admin@mail.bg","admin", authorities);
            this.userService.create(modelMapper.map(rootUser, UserServiceModel.class));
        }
    }
}
