package com.lucrum.enums;

public enum UserRole {
    USER,
    MODERATOR,
    ADMIN,
    ROOT
}