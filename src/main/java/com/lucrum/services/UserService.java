package com.lucrum.services;

import com.lucrum.entities.Role;
import com.lucrum.entities.User;
import com.lucrum.model.base.UserModel;
import com.lucrum.model.service.UserServiceModel;
import com.lucrum.model.view.UserEditViewModel;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Set;

public interface UserService extends UserDetailsService {
    boolean create(UserServiceModel userServiceModel);

    Set<UserServiceModel> getAll();

    UserServiceModel findUserById(String id);

    void deleteById(String id);

    UserServiceModel getUserByUsername(String username);

    boolean update(String id, UserEditViewModel userEditViewModel);

    Set<String> getUserRoleIds(UserModel userModel);

    Set<Role> getUserRoles (UserEditViewModel editViewModel);
}
