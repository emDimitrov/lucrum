package com.lucrum.services;

import com.lucrum.entities.Role;
import com.lucrum.enums.UserRole;

import java.util.Set;

public interface RoleService {
    boolean create(Role role);

    Role getRoleByUserRole(UserRole userRole);

    Role getRoleById(String id);

    Set<Role> getAll();

    Set<Role> getAllUserRoles();
}
