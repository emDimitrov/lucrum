package com.lucrum.services;

import com.lucrum.entities.Role;
import com.lucrum.entities.User;
import com.lucrum.enums.UserRole;
import com.lucrum.model.base.UserModel;
import com.lucrum.model.service.UserServiceModel;
import com.lucrum.model.view.UserEditViewModel;
import com.lucrum.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleService roleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder, RoleService roleService) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleService = roleService;
    }

    @Override
    public boolean create(UserServiceModel userServiceModel) {
        User userEntity = this.modelMapper.map(userServiceModel, User.class);
        userEntity.setPassword(bCryptPasswordEncoder.encode(userServiceModel.getPassword()));

        if (!this.getAll().isEmpty()){
            Set<Role> authorities = new HashSet<Role>(){{
                add(roleService.getRoleByUserRole(UserRole.USER));
            }};
            userEntity.setAuthorities(authorities);
        }

        try{
            this.userRepository.save(userEntity);
        }catch (DuplicateKeyException exception){
            System.out.println("An entity with already existing key cannot be saved");
            return false;
        }

        return true;
    }

    @Override
    public Set<UserServiceModel> getAll() {
        return this.userRepository.findAll()
                .stream()
                .map(x -> this.modelMapper.map(x, UserServiceModel.class))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public UserServiceModel findUserById(String id) {
        User userEntity = this.userRepository
                .findUserById(id)
                .orElse(null);

        return userEntity != null ?
                this.modelMapper.map(userEntity, UserServiceModel.class) :
                null;
    }

    @Override
    public void deleteById(String id) {
        if(this.userRepository.findUserById(id) != null){
            this.userRepository.deleteById(id);
        }
    }

    @Override
    public UserServiceModel getUserByUsername(String username) {
        User userEntity = this.userRepository
                .findByUsername(username)
                .orElse(null);

        return userEntity != null ?
                this.modelMapper.map(userEntity, UserServiceModel.class) :
                null;
    }

    @Override
    public boolean update(String id, UserEditViewModel userEditViewModel) {
        User userEntity = this.userRepository
                .findUserById(id)
                .orElse(null);
        if (userEditViewModel.getUserAuthorities()
                .contains(this.roleService.getRoleByUserRole(UserRole.ROOT)
                        .getId())){
            //:TODO Implement beter logic
            return false;
        }
        if (userEntity != null){
            this.modelMapper.map(userEditViewModel, userEntity);
            userEntity.setAuthorities(this.getUserRoles(userEditViewModel));

            return this.userRepository.save(userEntity) != null;
        }
            return false;
    }

    @Override
    public Set<String> getUserRoleIds(UserModel userModel) {
        Set<String> roles = new HashSet<>();
        for(Role role : userModel.getAuthorities()){
            roles.add(role.getId());
        }

        return roles;
    }

    @Override
    public Set<Role> getUserRoles(UserEditViewModel editViewModel) {
        Set<Role> userRoles = new HashSet<>();
        for(String roleId : editViewModel.getUserAuthorities()) {
            userRoles.add(roleService.getRoleById(roleId));
        }

        return userRoles;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User foundUser = this.userRepository
                .findByUsername(username)
                .orElse(null);

        if (foundUser == null) {
            throw new UsernameNotFoundException("Username not found.");
        }

        return foundUser;
    }
}
