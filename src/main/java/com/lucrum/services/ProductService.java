package com.lucrum.services;

import com.lucrum.model.service.ProductServiceModel;

import java.util.Set;

public interface ProductService {
    Set<ProductServiceModel> getAll();

    ProductServiceModel findProductById(String id);

    boolean create (ProductServiceModel categoryServiceModel);

    void deleteById(String id);

    boolean update (String id, ProductServiceModel categoryServiceModel);
}
