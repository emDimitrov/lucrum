package com.lucrum.services;

import com.lucrum.entities.Product;
import com.lucrum.model.service.ProductServiceModel;
import com.lucrum.repositories.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService{

    private final ModelMapper modelMapper;
    private final ProductRepository productRepository;

    public ProductServiceImpl(ModelMapper modelMapper, ProductRepository productRepository) {
        this.modelMapper = modelMapper;
        this.productRepository = productRepository;
    }

    @Override
    public Set<ProductServiceModel> getAll() {
        return this.productRepository.findAll()
                .stream()
                .map(x -> this.modelMapper.map(x, ProductServiceModel.class))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public ProductServiceModel findProductById(String id) {
        Product productEntity = this.productRepository
                .findProductById(id)
                .orElse(null);

        return productEntity != null ?
                this.modelMapper.map(productEntity, ProductServiceModel.class) :
                null;
    }

    @Override
    public boolean create(ProductServiceModel productServiceModel) {
        Product productEntity = modelMapper.map(productServiceModel, Product.class);
        try{
            return this.productRepository.save(productEntity) != null;
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }

    @Override
    public void deleteById(String id) {
        if (this.productRepository.findProductById(id) != null){
            this.productRepository.deleteById(id);
        }
    }

    @Override
    public boolean update(String id, ProductServiceModel productServiceModel) {
        Product productEntity = this.productRepository
                .findProductById(id)
                .orElse(null);

        if (productEntity != null){
            modelMapper.map(productServiceModel, productEntity);
            return this.productRepository.save(productEntity) != null;
        }
        return false;
    }
}
