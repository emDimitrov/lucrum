package com.lucrum.services;

import com.lucrum.entities.Category;
import com.lucrum.model.service.CategoryServiceModel;
import com.lucrum.repositories.CategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final ModelMapper modelMapper;
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(ModelMapper modelMapper, CategoryRepository categoryRepository) {
        this.modelMapper = modelMapper;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Set<CategoryServiceModel> getAll() {
        return this.categoryRepository.findAll()
                .stream()
                .map(x -> this.modelMapper.map(x, CategoryServiceModel.class))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public CategoryServiceModel findCategoryById(String id) {
        Category categoryEntity = this.categoryRepository
                .findCategoryById(id)
                .orElse(null);

        return categoryEntity != null ?
                this.modelMapper.map(categoryEntity, CategoryServiceModel.class) :
                null;
        }

    @Override
    public boolean create(CategoryServiceModel categoryServiceModel) {
        Category categoryEntity = modelMapper.map(categoryServiceModel, Category.class);
        try{
            return this.categoryRepository.save(categoryEntity) != null;
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }

    @Override
    public void deleteById(String id) {
        if (this.categoryRepository.findCategoryById(id) != null){
            this.categoryRepository.deleteById(id);
        }
    }

    @Override
    public boolean update(String id, CategoryServiceModel categoryServiceModel) {
        Category categoryEntity = this.categoryRepository
                .findCategoryById(id)
                .orElse(null);

        if (categoryEntity != null){
            modelMapper.map(categoryServiceModel, categoryEntity);
            return this.categoryRepository.save(categoryEntity) != null;
        }
        return false;
    }
}
