package com.lucrum.services;

import com.lucrum.entities.Category;
import com.lucrum.model.service.CategoryServiceModel;

import java.util.Set;

public interface CategoryService {
    Set<CategoryServiceModel> getAll();

    CategoryServiceModel findCategoryById(String id);

    boolean create (CategoryServiceModel categoryServiceModel);

    void deleteById(String id);

    boolean update (String id, CategoryServiceModel categoryServiceModel);
}
