package com.lucrum.services;

import com.lucrum.entities.Role;
import com.lucrum.enums.UserRole;
import com.lucrum.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public boolean create(Role role) {
        return this.roleRepository.save(role) != null;
    }

    @Override
    public Role getRoleByUserRole(UserRole userRole) {
        return this.roleRepository.findRoleByAuthority(userRole.name());
    }

    @Override
    public Role getRoleById(String id) {
        return this.roleRepository.findRoleById(id);
    }

    @Override
    public Set<Role> getAll() {
        return this.roleRepository
                .findAll()
                .stream()
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<Role> getAllUserRoles() {
        Set<Role> userRoles = this.roleRepository
                .findAll()
                .stream()
                .collect(Collectors.toCollection(LinkedHashSet::new));
        userRoles.remove(getRoleByUserRole(UserRole.ROOT));

        return userRoles;
    }
}
