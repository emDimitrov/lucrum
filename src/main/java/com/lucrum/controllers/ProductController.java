package com.lucrum.controllers;

import com.lucrum.entities.Product;
import com.lucrum.model.binding.ProductBindingModel;
import com.lucrum.model.service.ProductServiceModel;
import com.lucrum.model.view.ProductViewModel;
import com.lucrum.services.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/product")
public class ProductController extends BaseController{

    private final ModelMapper modelMapper;
    private final ProductService productService;

    public ProductController(ModelMapper modelMapper, ProductService productService) {
        this.modelMapper = modelMapper;
        this.productService = productService;
    }

    @GetMapping("/all")
    public ModelAndView getAll(){
        return this.view("views/product/all", this.productService.getAll(),"Products");
    }

    @GetMapping("/add")
    public ModelAndView getCreate(@ModelAttribute ProductBindingModel productBindingModel){
        return this.view("views/product/add",null,"Create Product");
    }

    @PostMapping("/add")
    public ModelAndView create(@ModelAttribute ProductBindingModel productBindingModel){
        this.productService.create(modelMapper.map(productBindingModel, ProductServiceModel.class));
        return this.view("views/product/all", this.productService.getAll(),"Products");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView getEdit(@PathVariable String id, @ModelAttribute ProductViewModel productViewModel){
        this.modelMapper.map(this.productService.findProductById(id), productViewModel);

        return this.view("views/product/edit", productViewModel, "Edit Product");
    }

    @PostMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable String id, @ModelAttribute ProductBindingModel productBindingModel){
        this.productService.update(id, modelMapper.map(productBindingModel,ProductServiceModel.class));

        return this.view("views/product/all", this.productService.getAll(), "Products");
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable String id){
        this.productService.deleteById(id);

        return this.view("views/product/all", this.productService.getAll(), "Products");
    }
}
