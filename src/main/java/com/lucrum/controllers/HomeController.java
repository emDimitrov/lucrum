package com.lucrum.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class HomeController extends BaseController{
    @GetMapping("/")
    public ModelAndView index(){
        return this.plainView("views/index");
    }

    @GetMapping("/home")
    @PreAuthorize("!isAnonymous()")
    public ModelAndView home(Principal principal) {
        return this.view("views/home", principal.getName(),"Lucrum");
    }
}
