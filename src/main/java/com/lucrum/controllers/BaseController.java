package com.lucrum.controllers;

import org.springframework.web.servlet.ModelAndView;

public class BaseController {
    protected ModelAndView view(String viewName, Object viewModel, String title) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("layout");
        modelAndView.addObject("viewName", viewName);
        modelAndView.addObject("viewModel", viewModel);
        modelAndView.addObject("title", title);

        return modelAndView;
    }
    protected ModelAndView view(String viewName) {
        return this.view(viewName, null, null);
    }

    protected ModelAndView view(String viewName, Object viewModel) {
        return this.view(viewName, viewModel);
    }

    protected ModelAndView redirect(String redirectUrl) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("redirect:" + redirectUrl);

        return modelAndView;
    }

    protected ModelAndView plainView(String viewName){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(viewName);

        return modelAndView;
    }
}
