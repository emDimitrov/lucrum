package com.lucrum.controllers;

import com.lucrum.model.binding.user.UserEditBindingModel;
import com.lucrum.model.service.UserServiceModel;
import com.lucrum.model.view.UserEditViewModel;
import com.lucrum.services.RoleService;
import com.lucrum.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController extends BaseController{

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final RoleService roleService;

    @Autowired
    public AdminController(UserService userService, ModelMapper modelMapper, RoleService roleService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.roleService = roleService;
    }

    @GetMapping("/user/all")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView getAll(){
        return this.view("views/user/all", this.userService.getAll(),"Users");
    }

    @GetMapping("/user/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView getEdit(@PathVariable String id, @ModelAttribute UserEditViewModel viewModel){
        UserServiceModel userServiceModel = this.userService.findUserById(id);
        modelMapper.map(userServiceModel,viewModel);
        viewModel.setUserAuthorities(this.userService.getUserRoleIds(userServiceModel));

        return this.view("views/user/edit", viewModel, "Users")
                .addObject("roles", this.roleService.getAllUserRoles());
    }

    @PostMapping("/user/edit/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView edit(@PathVariable String id, @ModelAttribute UserEditBindingModel bindingModel){
        UserEditViewModel userModel = new UserEditViewModel();
        modelMapper.map(bindingModel,userModel);
        userModel.setUserAuthorities(this.userService.getUserRoleIds(bindingModel));

        this.userService.update(id, userModel);

        return this.view("views/user/all", this.userService.getAll(), "Users");
    }

    @GetMapping("/user/delete/{id}")
    @PreAuthorize("hasAnyAuthority('ROOT', 'ADMIN')")
    public ModelAndView delete(@PathVariable String id){

        this.userService.deleteById(id);
        return this.view("views/user/all", null,"Users")
                .addObject("users",this.userService.getAll());
    }
}
