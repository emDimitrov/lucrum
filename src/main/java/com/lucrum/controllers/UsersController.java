package com.lucrum.controllers;

import com.lucrum.model.binding.user.UserRegisterBindingModel;
import com.lucrum.model.service.UserServiceModel;
import com.lucrum.services.RoleService;
import com.lucrum.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UsersController extends BaseController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public UsersController(UserService userService, RoleService roleService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/login")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getLogin() {
        return this.view("views/user/login",null,"Login");
    }

    @GetMapping("/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getRegister(){
        return this.view("views/user/register");
    }

    @PostMapping("/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView register(@ModelAttribute UserRegisterBindingModel bindingModel){
        if(!bindingModel.getPassword()
                .equals(bindingModel.getConfirmPassword())) {
            return this.view("register");
        }
        this.userService.create(modelMapper.map(bindingModel, UserServiceModel.class));

        return this.view("views/user/login");
    }
}

