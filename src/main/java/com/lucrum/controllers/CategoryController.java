package com.lucrum.controllers;

import com.lucrum.entities.Category;
import com.lucrum.model.binding.CategoryBindingModel;
import com.lucrum.model.service.CategoryServiceModel;
import com.lucrum.model.view.CategoryEditViewModel;
import com.lucrum.services.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/category")
public class CategoryController extends BaseController {

    private final ModelMapper modelMapper;
    private final CategoryService categoryService;

    public CategoryController(ModelMapper modelMapper, CategoryService categoryService) {
        this.modelMapper = modelMapper;
        this.categoryService = categoryService;
    }

    @GetMapping("/all")
    public ModelAndView getAll(){
        return this.view("views/category/all", this.categoryService.getAll(),"Categories");
    }

    @GetMapping("/add")
    public ModelAndView getCreate(@ModelAttribute CategoryBindingModel categoryBindingModel){
        return this.view("views/category/add", null, "Add Category");
    }

    @PostMapping("/add")
    public ModelAndView create(@ModelAttribute CategoryBindingModel categoryBindingModel){
        this.categoryService.create(modelMapper.map(categoryBindingModel, CategoryServiceModel.class));
        return this.view("views/category/all", this.categoryService.getAll(),"Categories");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView getUpdate(@PathVariable String id, @ModelAttribute CategoryEditViewModel viewModel){
        this.modelMapper.map(this.categoryService.findCategoryById(id), viewModel);

        return this.view("views/category/edit", viewModel, "Edit Category");
    }

    @PostMapping("/edit/{id}")
    public ModelAndView update(@PathVariable String id, @ModelAttribute CategoryBindingModel bindingModel){
        this.categoryService.update(id, modelMapper.map(bindingModel,CategoryServiceModel.class));

        return this.view("views/category/all", this.categoryService.getAll(), "Categories");
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable String id) {
        this.categoryService.deleteById(id);

        return this.view("views/category/all", this.categoryService.getAll(), "Categories");
    }
}
