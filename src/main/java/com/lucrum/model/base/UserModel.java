package com.lucrum.model.base;

import com.lucrum.entities.Role;

import java.util.Set;

public interface UserModel {
    Set<Role> getAuthorities();
}
